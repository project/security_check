Security Check

This is a drush plugin for running security checks against Drupal sites.

Requirements:
As you can run the security checks against your Dupal site using the Drush command. So Drush Should be installed.

For the installation of this module you can simply download it from https://www.drupal.org/project/security_check. Extract the downloaded file and paste it in sites >> all >> modules.

Then, For the installation go to the admin/modules. find the Security Check module in the list and install it.

These are basic security checks, they aren't intended to replace more through reviews of a site build. The tests are designed to stop a badly misconfigured site going live.

To run this against a site, simply run "drush secchk" from the docroot or using a site alias.
